require 'mechanize'
require 'byebug'
require 'sqlite3'
require 'net/smtp'
require './config.rb'
require 'textmagic-ruby'

agent = Mechanize.new

# LOGINS = [{
#    username: '',
#    password: '',
#    dob: '',
#    lastname: ''
#}]

LOGINS.each do |login|
  login_page = agent.get('https://www.ispfsb.com/Public/Login.aspx')

  login_form = login_page.form_with(:id => 'form1')

  login_form['ctl00$ContentPlaceHolder1$txtU'] = login[:username]
  login_form['ctl00$ContentPlaceHolder1$txtP'] = login[:password]
  login_form['ctl00$ContentPlaceHolder1$txtLastname'] = login[:lastname]
  login_form['ctl00$ContentPlaceHolder1$txtDOB'] = login[:dob]

  login_form['ctl00$ContentPlaceHolder1$btnSignIn.x'] = 71
  login_form['ctl00$ContentPlaceHolder1$btnSignIn.y'] = 12

  #login_form.action = asp_link_args.values_at(action_arg) if action_arg
  #dashboard = agent.click(login_page.input_with(:name => 'ctl00$ContentPlaceHolder1$btnSignIn'))
  dashboard = login_form.submit

  foid_status = dashboard.search('//label[@id="myFOIDStatus"]/span').text
  ccl_status = dashboard.search('//label[@id="myCCLStatus"]/span').text
  cert_status = dashboard.search('//label[@id="myCERTStatus"]/span').text

  puts ''
  puts "Checking #{login[:username]}..."
  puts Time.now.strftime("%m/%d/%Y %H:%M")
  puts "FOID: #{foid_status}"
  puts "CCW: #{ccl_status}"
  puts "Instructor: #{cert_status}"


  db = SQLite3::Database.open 'foidcheck.db'
  db.execute 'CREATE TABLE IF NOT EXISTS checks(username TEXT PRIMARY KEY, foid_status TEXT, foid_date INT, ccw_status TEXT, ccw_date INT)'
  begin
    db.execute 'ALTER TABLE checks ADD COLUMN sms TEXT'
  rescue
  end

  alert = ''
  short_alert = '';

  now = Date.today.to_time.to_i
  last_check = db.get_first_row('SELECT * FROM checks WHERE username = ?', login[:username])

  if last_check.nil?
    sql = 'INSERT INTO checks (username, foid_status, foid_date, ccw_status, ccw_date) VALUES (?,?,?,?,?)'
    db.execute(sql, login[:username], foid_status, now, ccl_status, now)

    alert = "Thank you for using foidcheck!\n\n" +
            "Check performed for #{login[:username]}  at #{Time.now.strftime("%m/%d/%Y %H:%M")}\n" +
            "FOID: #{foid_status}\n" +
            "CCW: #{ccl_status}\n" +
            "Instructor: #{cert_status}\n" +
            "\n\n\n" +
            "You will be notified once one of these statuses has changed."
    short_alert = 'Thank you for using foidcheck. You will be notified once your status changes.'
    
  elsif foid_status == '' and ccl_status == '' and cert_status == '' then
      # nothing to do - the checks did not work
  else 
    unless last_check[1] == foid_status
      alert = "#{alert}\nFOID status updated: #{foid_status}"
      sql = 'UPDATE checks SET foid_status=?, foid_date=? WHERE username=?'
      db.execute(sql, foid_status, now, login[:username])
      short_alert = short_alert + " FOID status updated: #{foid_status}"
    end
    unless last_check[3] == ccl_status
      alert = "#{alert}\nCCW status updated: #{ccl_status}"
      sql = 'UPDATE checks SET ccw_status=?, ccw_date=? WHERE username=?'
      db.execute(sql, ccl_status, now, login[:username])
      short_alert = short_alert + " CCW status updated: #{ccl_status}"
    end
  end
  if alert == ''
    alert = "#{login[:alert]}: no change"
  else
    mail_to = login[:alert]
    message = ''
    message = message + "From: foidnotify@bedrosian.com\n"
    message = message + "To: #{mail_to}\n"

    message = message + "Subject: #{login[:username]} FOID/CCW Status changed!\n"
    message = message + "\n"


    message = message + alert

    begin
      puts "Sending email to #{mail_to}..."
      Net::SMTP.start(CONFIG_MAIL_SERVER, CONFIG_MAIL_PORT, CONFIG_MAIL_DOMAIN, CONFIG_MAIL_USER, CONFIG_MAIL_PASSWORD) do |smtp|
        smtp.send_message message, 'foidcheck@bedrosian.com', mail_to, 'foidcheck@bedrosian.com', 'charles@bedrosian.com'    
      end
      puts 'Email sent!'
    rescue Exception => e
      puts 'Email failed: ' + e.message
    end

    if defined?(login[:sms]) and defined?(CONFIG_TEXTMAGIC_USERNAME) and defined?(CONFIG_TEXTMAGIC_KEY)
      begin
        sms_to = login[:sms]
        puts "Sending SMS notification to #{sms_to}..."
        client = Textmagic::REST::Client.new CONFIG_TEXTMAGIC_USERNAME, CONFIG_TEXTMAGIC_KEY
        params = {:phones => sms_to, :text => short_alert.strip}
        message = client.messages.create(params)
      rescue Exception => e
        puts 'SMS Failed: ' + e.message
      end
    else
      puts 'Skipping SMS notification...'
    end
  end

  puts alert


end
