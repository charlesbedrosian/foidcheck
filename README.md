Want to be notified when your FOID or CCW is issued?

Run this script in a CRON job.  Your login should be stored in config.rb and the script is simply executed as `ruby foidcheck.rb`.  Also update the config.rb with your mail server settings.

config.rb:

```
# can have multiple logins
LOGINS = [
    {
        username: 'unclesam@gmail.com',
        password: 'supersecret',
        dob: '07/04/1776',
        lastname: 'america',
        sms: '18475551212'
    }
]

CONFIG_MAIL_SERVER = 'smtp.gmail.com'
CONFIG_MAIL_PORT = 587
CONFIG_MAIL_USER = 'unclesam@gmail.com'
CONFIG_MAIL_PASSWORD = 'supersecret'

# remove these keys to skip SMS notification
# or create an account and API key at http://textmagic.com
CONFIG_TEXTMAGIC_KEY = 'textmagic_api_key'
CONFIG_TEXTMAGIC_USERNAME = 'textmagic_username'

```
